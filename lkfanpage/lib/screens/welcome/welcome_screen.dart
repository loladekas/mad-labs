import 'dart:async';
import 'package:flutter/material.dart';
import 'package:lkfanpage/auth_controller.dart';
import 'package:lkfanpage/screens/componeents/body.dart';
import 'package:flutter/src/material/icon_button.dart';

import '../../comments.dart';

class commentScreen extends StatelessWidget {
  createAlertDialog(BuildContext context) {
    TextEditingController customController = new TextEditingController();
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
              title: Text('Post a message'),
              content: TextField(
                controller: customController,
              ),
              actions: <Widget>[
                MaterialButton(
                  elevation: 5.0,
                  child: Text('Submit'),
                  onPressed: () {
                    Navigator.of(context).pop(customController.text.toString());

                    createAlertDialog(context);
                  },
                )
              ]);
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Post a message'),
        ),
        body: Builder(builder: (context) {
          return Container(
            child: Center(
              child: FlatButton(
                padding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0)),
                color: Colors.deepOrange,
                textColor: Colors.white,
                onPressed: () {
                  createAlertDialog(context);
                },
                child: Text(
                  'X',
                  style: TextStyle(fontSize: 20.0),
                ),
              ),
            ),
          );
        }));
  }
}

class messageScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'FanPage',
      home: Scaffold(
          appBar: AppBar(title: Center(child: Text('Fanpage'))), body: Post()),
    );
  }
}

class CircleImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 45.0,
      height: 45.0,
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
            fit: BoxFit.fill,
            image: AssetImage(
                '/Users/loladekasumu/Developer/MobileApp/lkfanpage/assests/images/IMG_7070.png'),
          )),
    );
  }
}

class PostHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(10.0),
        child: Row(children: [
          CircleImage(),
          Container(
              padding: EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
              child: Text('LoladeKasumu',
                  style: TextStyle(
                      fontFamily: 'Roboto',
                      fontSize: 17.0,
                      fontWeight: FontWeight.w600)))
        ]));
  }
}

class Post extends StatefulWidget {
  @override
  PostState createState() => new PostState();
}

class PostState extends State<Post> {
  bool liked = false;
  bool showHeartOverlay = false;

  _pressed() {
    setState(() {
      liked = !liked;
    });
  }

  _doubleTapped() {
    setState(() {
      showHeartOverlay = true;
      liked = true;
      if (showHeartOverlay) {
        Timer(const Duration(milliseconds: 500), () {
          setState(() {
            showHeartOverlay = false;
          });
        });
      }
    });
  }

  _commentButtonPressed() {
    setState(() {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => messageScreen()));
    });
  }

  @override
  Widget build(BuildContext context) {
    IconButton heartButton = IconButton(
      iconSize: 35.0,
      icon: Icon(liked ? Icons.favorite : Icons.favorite_border,
          color: liked ? Colors.red : null),
      tooltip: 'Increase volume by 10%',
      onPressed: () => _pressed(),
    );

    IconButton exitButton = IconButton(
        iconSize: 34.0,
        icon: Icon(Icons.home, color: Colors.grey),
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => WelcomeScreen(
                    email: '',
                  )));
        });

    IconButton commentButton = IconButton(
      iconSize: 35.0,
      icon: Icon(Icons.chat_bubble_outline, color: Colors.grey),
      onPressed: () {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => messageScreen()));
      },
    );

    return Container(
        child: Column(
      children: [
        PostHeader(),
        GestureDetector(
          onDoubleTap: () => _doubleTapped(),
          child: Stack(
            alignment: Alignment.center,
            children: <Widget>[
              Image.asset(
                  '/Users/loladekasumu/Developer/MobileApp/lkfanpage/assests/images/mald.png'),
              showHeartOverlay
                  ? Icon(Icons.favorite, color: Colors.white, size: 80.0)
                  : Container()
            ],
          ),
        ),
        ListTile(contentPadding: EdgeInsets.all(0.0), leading: heartButton),
        ListTile(contentPadding: EdgeInsets.all(0.0), leading: commentButton),
        ListTile(contentPadding: EdgeInsets.all(0.0), leading: exitButton)
      ],
    ));
  }
}

class WelcomeScreen extends StatelessWidget {
  String email;
  WelcomeScreen({Key? key, required this.email}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double w = MediaQuery.of(context).size.width;
    double h = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          Container(
              width: w,
              height: h * 0.3,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assests/images/bluebackground.png"),
                      fit: BoxFit.cover)),
              child: Column(children: [
                SizedBox(
                  height: h * 0.18,
                ),
                CircleAvatar(
                  backgroundColor: Colors.white70,
                  radius: 40,
                  backgroundImage: AssetImage("assests/images/photoicon.png"),
                )
              ])),
          Container(
            margin: const EdgeInsets.only(left: 20, right: 20),
            width: w,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 50,
                ),
                SizedBox(
                  height: 20,
                ),
              ],
            ),
          ),
          SizedBox(
            height: 70,
          ),
          Container(
            width: w,
            margin: const EdgeInsets.only(left: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Welcome",
                  style: TextStyle(
                      fontSize: 36,
                      fontWeight: FontWeight.bold,
                      color: Colors.black54),
                ),
                Text(
                  email,
                  style: TextStyle(fontSize: 18, color: Colors.grey[500]),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 100,
          ),
          GestureDetector(
            onTap: () {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => messageScreen()));
            },
            child: Container(
              width: w * 0.5,
              height: h * 0.08,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  image: DecorationImage(
                      image: AssetImage("assests/images/bluebackground.png"),
                      fit: BoxFit.cover)),
              child: Center(
                child: Text(
                  "Home",
                  style: TextStyle(
                    fontSize: 36,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          GestureDetector(
            onTap: () {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => commentScreen()));
            },
            child: Container(
              width: w * 0.5,
              height: h * 0.08,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  image: DecorationImage(
                      image: AssetImage("assests/images/bluebackground.png"),
                      fit: BoxFit.cover)),
              child: Center(
                child: Text(
                  "message",
                  style: TextStyle(
                    fontSize: 36,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          GestureDetector(
            onTap: () {
              AuthController.instance.logOut();
            },
            child: Container(
              width: w * 0.5,
              height: h * 0.08,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  image: DecorationImage(
                      image: AssetImage("assests/images/bluebackground.png"),
                      fit: BoxFit.cover)),
              child: Center(
                child: Text(
                  "Log out",
                  style: TextStyle(
                    fontSize: 36,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
          SizedBox(height: w * 0.02),
        ],
      ),
    );
  }
}
