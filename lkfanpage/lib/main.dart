import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:lkfanpage/auth_controller.dart';
import 'package:lkfanpage/constants.dart';
import 'package:lkfanpage/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:lkfanpage/screens/componeents/body.dart';
import 'package:lkfanpage/screens/signup_page.dart';
import 'package:lkfanpage/screens/welcome/welcome_screen.dart';
import 'package:get/get.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:lkfanpage/splashscreen.dart';
import 'google_sign_in.dart';
import 'package:provider/provider.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp().then((value) => Get.put(AuthController()));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter',
      theme: ThemeData(),
      home: SplashScreen(),
    );
  }
}
