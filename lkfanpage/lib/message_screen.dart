import 'package:flutter/material.dart';
import 'package:lkfanpage/main.dart';

// void main() => runApp(MyApp());

// class messageScreen extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'FanPage',
//       home: Scaffold(
//           appBar: AppBar(title: Center(child: Text('Fanpage'))),
//           body: Column(
//             children: <Widget>[
//               Container(
//                   padding: EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
//                   child: Row(
//                     children: <Widget>[
//                       CircleImage(),
//                       Container(
//                           padding: EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
//                           child: Text(
//                             'Loladekasumu',
//                             style: TextStyle(
//                                 fontFamily: 'Roboto',
//                                 fontSize: 17.0,
//                                 fontWeight: FontWeight.w600),
//                           ))
//                     ],
//                   )),
//               Image.asset(
//                   '/Users/loladekasumu/Developer/MobileApp/lkfanpage/assests/images/mald.png'),
//               ListTile(leading: Icon(Icons.favorite, color: Colors.red))
//             ],
//           )),
//     );
//   }
// }

// class CircleImage extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       width: 45.0,
//       height: 45.0,
//       decoration: BoxDecoration(
//           shape: BoxShape.circle,
//           image: DecorationImage(
//               fit: BoxFit.fill,
//               image: AssetImage(
//                   '/Users/loladekasumu/Developer/MobileApp/lkfanpage/assests/images/IMG_7070.png'))),
//     );
//   }
// }

class messageScreen extends StatelessWidget {
  createAlertDialog(BuildContext context) {
    TextEditingController customController = new TextEditingController();
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
              title: Text('Post a message'),
              content: TextField(
                controller: customController,
              ),
              actions: <Widget>[
                MaterialButton(
                  elevation: 5.0,
                  child: Text('Submit'),
                  onPressed: () {
                    Navigator.of(context).pop(customController.text.toString());

                    createAlertDialog(context);
                  },
                )
              ]);
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Post a message'),
        ),
        body: Builder(builder: (context) {
          return Container(
            child: Center(
              child: FlatButton(
                padding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0)),
                color: Colors.deepOrange,
                textColor: Colors.white,
                onPressed: () {
                  createAlertDialog(context);
                },
                child: Text(
                  'X',
                  style: TextStyle(fontSize: 20.0),
                ),
              ),
            ),
          );
        }));
  }
}
