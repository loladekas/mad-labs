import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:lkmessageapp/helperfunctions/authenticate.dart';
import 'package:lkmessageapp/screens/chatRoomScreen.dart';
import 'package:lkmessageapp/screens/rating.dart';
import 'package:lkmessageapp/screens/signin.dart';
import 'package:lkmessageapp/screens/signup.dart';

import 'helperfunctions/helperfunctions.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Future<FirebaseApp> _initializer = Firebase.initializeApp();
  bool userIsLoggedIn = false;

  @override
  void initState() {
    getLoggedInState();
    super.initState();
  }

  getLoggedInState() async {
    await HelperFunctions.getUserLoggedInSharedPreference().then((value) {
      setState(() {
        userIsLoggedIn = value!;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: Color(0xff145C9E),
          scaffoldBackgroundColor: Color(0xff1F1F1F),
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: FutureBuilder<FirebaseApp>(
            future: _initializer,
            builder:
                (BuildContext context, AsyncSnapshot<FirebaseApp> snapshot) {
              if (snapshot.hasError) {
                return const Center(
                  child: Text("opps somethings wrong w firebase set up"),
                );
              }
              if (snapshot.connectionState == ConnectionState.done) {
                return userIsLoggedIn ? ChatRoom() : Authenticate();
              } else {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
            }));
  }
}
