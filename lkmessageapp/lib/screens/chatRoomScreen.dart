import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:lkmessageapp/helperfunctions/authenticate.dart';
import 'package:lkmessageapp/helperfunctions/constants.dart';
import 'package:lkmessageapp/helperfunctions/helperfunctions.dart';
import 'package:lkmessageapp/screens/search.dart';
import 'package:lkmessageapp/screens/signin.dart';
import 'package:lkmessageapp/services/auth.dart';
import 'package:lkmessageapp/services/database.dart';
import 'package:lkmessageapp/widgets/widget.dart';

import 'conversation_screen.dart';

class ChatRoom extends StatefulWidget {
  @override
  _ChatRoomState createState() => _ChatRoomState();
}

class _ChatRoomState extends State<ChatRoom> {
  AuthMethod authMethods = new AuthMethod();
  DatabaseMethods databaseMethods = new DatabaseMethods();

  Stream<QuerySnapshot<Map<String, dynamic>>>? chatRoomsStream;

  Widget chatRoomList() {
    return StreamBuilder(
      stream: chatRoomsStream,
      builder: (BuildContext context,
          AsyncSnapshot<QuerySnapshot<Map<String, dynamic>>> snapshot) {
        // if (snapshot.hasData) {
        return snapshot.hasData
            ? ListView.builder(
                itemCount: snapshot.data?.docs.length,
                itemBuilder: (context, index) {
                  print(snapshot.data!.docs[index]);
                  // print(snapshot.data);
                  return ChatRoomsTile(
                      snapshot.data!.docs[index]
                          .data()["chatRoomId"]
                          .toString()
                          .replaceAll("_", "")
                          .replaceAll(Constants.myName, ""),
                      snapshot.data!.docs[index].data()["chatRoomId"]);
                })
            : Container();
      },
    );
  }

  @override
  void initState() {
    getUserInfo();
    super.initState();
  }

  getUserInfo() async {
    var result = await HelperFunctions.getUserNameSharedPreference();
    if (result != null) {
      Constants.myName = result;
      setState(() {
        chatRoomsStream = databaseMethods.getChatRooms(Constants.myName);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // title: CircleAvatar(
        //     backgroundImage: AssetImage('assets/images/kim.png'), radius: 50),
        actions: [
          GestureDetector(
            onTap: () {
              authMethods.signOut();
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => Authenticate()));
            },
            child: Container(
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: Icon(Icons.exit_to_app)),
          )
        ],
      ),
      body: chatRoomList(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.search),
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => SearchScreen()));
        },
      ),
    );
  }
}

class ChatRoomsTile extends StatelessWidget {
  final String userName;
  final String chatRoomId;

  ChatRoomsTile(this.userName, this.chatRoomId);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ConversationScreen(chatRoomId)));
      },
      child: Container(
        color: Colors.black26,
        padding: EdgeInsets.symmetric(horizontal: 24, vertical: 16),
        height: 40,
        width: 40,
        alignment: Alignment.center,
        child: Row(children: [
          Container(
            decoration: BoxDecoration(
                color: Colors.blue, borderRadius: BorderRadius.circular(40)),
            child: Text(
              "${userName.substring(0, 1).toUpperCase()}",
              style: mediumTextFieldStyle(),
            ),
          ),
          SizedBox(
            width: 8,
          ),
          Text(
            userName,
            style: mediumTextFieldStyle(),
          )
        ]),
      ),
    );
  }
}
